#!/usr/bin/env bash

# The lsblk command outputs this, which is pretty straightforward
# to determine which disk is not formatted (no partitions).
# sdb          disk
# sda14        part
# sda15 vfat   part
# sda1  ext4   part


disk=`sudo lsblk -o NAME,FSTYPE,TYPE -dsn | grep disk | cut -d " " -f 1`

if [[ -z "$disk" ]]; then
  echo "No unformatted disks"
elif [[ -n "$disk" ]]; then
  # check if it says TYPE=disk and FSTYPE is now in place
  cmd="sudo lsblk /dev/$disk -o FSTYPE -dsn"
  fstype=`$cmd`

  if [[ -z "$fstype" ]]; then
    cmd="sudo mkfs.ext4 -m 0 -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/$disk"
    echo "$cmd"
  
    results=`$cmd`
    echo "$results"
  else
    echo "Already formatted with fstype $fstype"
  fi

fi

