data "aws_ami" "latest_ubuntu_16_04" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}



module "ec2_psql" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "psql"
  instance_count         = 1

  ami                    = data.aws_ami.latest_ubuntu_16_04.id
  instance_type          = "t2.medium"
  key_name               = "ec2-terransible"
  monitoring             = true
  vpc_security_group_ids = [module.security_group_psql.this_security_group_id]
  #subnet_id              = "subnet-eddcdzz4"
  subnet_ids                = module.vpc.public_subnets
  associate_public_ip_address = true

  root_block_device = [
      {
        volume_type = "gp2"
        volume_size = 10
      },
    ]

    ebs_block_device = [
      {
        device_name = "/dev/sdf"
        volume_type = "gp2"
        volume_size = 40
      }
    ]
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

# write hosts file for store
# This adds the external IP addresses of each vm to the ansible hosts file
data "template_file" "anshosts" {
  template = file("./templates/hosts.tpl")

  vars = {
    psql_info     = "${module.ec2_psql.public_ip.0} ansible_python_interpreter=/usr/bin/python3"
  }
}

resource "local_file" "host_file" {
  depends_on = [module.ec2_psql.public_ip]

  content  = data.template_file.anshosts.rendered
  filename = "./hosts"
}

# Just informational, this is the external IP address of the psql vm
output "ec2_psql_public_ip" {
  value = module.ec2_psql.public_ip.0
}
