# These terraform and ansible scripts do the following:
  * Setup a kubernetes cluster in AWS EKS
  * Setup an ec2 instance as a shared PostgreSQL database Server
    - This server also has a 2nd attached disk for persistent storage
    - This DB server is currently externally available, but it's also configured on the private subnet, so that can be locked down and still allow apps and services within the cluster to use the database.
    - This ec2 instance is using an old version of Ubuntu (Ubuntu 16.04) and PostgreSQL for compatibility.  Both need to be upgraded to latest versions.
  * Deploys multiple apps into the cluster:
    - My Covid19 Tracker Rails App can be viewed at: http://a728d3efabf204f7db6f12893e74e899-1138385648.us-east-2.elb.amazonaws.com:3000
    - My Kubernetes (k8s) Test Rails App can be viewed at: http://a1a2f7abfb1de4f428ef3d8a403b3794-294262798.us-east-2.elb.amazonaws.com:3000
    - Kubernetes Dashboard: (first run kubectl proxy): http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/error?namespace=default

In the interest of time, I currently manually create a LoadBalancer (see below) for each deployed Rails application using a one-line kubectl command for each.  This should be instead be done in Terraform/Ansible.  The real solution would be to configure DNS subdomains as I did for `gcp`.  That should be an easy next step, though.

# To replicate this in AWS do the following:

[AWS SETUP]
1. Install awscli (if needed)
2. Run aws configure (if needed)
   * For this project I set the default region to us-east-2
3. I recommend setting up AWS Config to aggregate under one dashboard all of the resources that you're using in your region
   * https://us-east-2.console.aws.amazon.com/config
4. Make changes if necessary to ./ansible/ansible.cfg and ./ansible/ansible.variables.common.yml

[TERRAFORM]
1. cd to ./terransible-template/aws/iac-provision-aws-eks-cluster
2. run: terraform init
3. run: terraform plan
4. run: terraform apply
5. (Needs to be added to Terraform, but manual for now): Configure kubectl to add your new cluster to .kube config:
  * `aws eks --region $(terraform output region) update-kubeconfig --name $(terraform output cluster_name)`
6. (Needs to be added to Terraform, but manual for now):
  * Add metrics server
  * Add Kubernetes Dashboard
  * You can run `k8s-token.sh` to generate the token needed to login to dashboard

[ANSIBLE]
1. cd to ansible
2. run: ansible-playbook site.yml

[ERRATA / IMPROVEMENTS]

* If the pods aren't deploying, make sure the container registry is authorized properly.  You may need to follow steps below under registry secrets.  The registry secrets part needs to be automated in Terraform.
* I didn't configure custom subdomains yet, like I did for *.dev-gcp.peterleiser.com.  This needs to be done as an improvement.
  - So for now, you need to create a Load Balancer for each service (e.g. covid19-tracker and ks8-test apps) and expose it (see instructions below)


# Create a Load Balancer and expose the pods

https://kubernetes.io/docs/tutorials/stateless-application/expose-external-ip-address/

```
kubectl expose deployment k8s-test-deployment -n dev-aws-peterleiser-dot-com-apps --type=LoadBalancer --name=lb-k8s-test-service

kubectl expose deployment covid19-tracker-deployment -n dev-aws-peterleiser-dot-com-apps --type=LoadBalancer --name=lb-covid19-tracker-service
```



Then you can get the IP addresses:

```
➜  ansible git:(master) ✗ kubectl get services -A
NAMESPACE                          NAME                         TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)          AGE
default                            kubernetes                   ClusterIP      172.20.0.1       <none>                                                                    443/TCP          15h
dev-aws-peterleiser-dot-com-apps   covid19-tracker-service      ClusterIP      172.20.30.94     <none>                                                                    3000/TCP         144m
dev-aws-peterleiser-dot-com-apps   k8s-test-service             ClusterIP      172.20.9.15      <none>                                                                    3000/TCP         144m
dev-aws-peterleiser-dot-com-apps   lb-covid19-tracker-service   LoadBalancer   172.20.239.252   a728d3efabf204f7db6f12893e74e899-1138385648.us-east-2.elb.amazonaws.com   3000:31135/TCP   39m
dev-aws-peterleiser-dot-com-apps   lb-k8s-test-service          LoadBalancer   172.20.149.172   a1a2f7abfb1de4f428ef3d8a403b3794-294262798.us-east-2.elb.amazonaws.com    3000:32142/TCP   82m
kube-system                        kube-dns                     ClusterIP      172.20.0.10      <none>                                                                    53/UDP,53/TCP    15h
kube-system                        metrics-server               ClusterIP      172.20.225.62    <none>                                                                    443/TCP          14h
kubernetes-dashboard               dashboard-metrics-scraper    ClusterIP      172.20.230.157   <none>                                                                    8000/TCP         14h
kubernetes-dashboard               kubernetes-dashboard         ClusterIP      172.20.88.119    <none>                                                                    443/TCP          14h
```

# For registry secrets:

1. Create the registry secret manually on the command line using:

```
create secret docker-registry gl-covid19-tracker-token \
  --docker-server registry.gitlab.com \
  --docker-username gitlab+deploy-token-dev-gcp-1 \
  --docker-email docker.com@peterleiser.com \
  --docker-password <gitlab_token_password>
```

2. Then get the generated secret:

```
kubectl -n=dev-aws-peterleiser-dot-com-apps get secret gl-covid19-tracker-token -o yaml > dockerconfigjson.covid19-tracker.dev-aws
```

3. Take the .dockerconfigjson value from the yaml you just output and put that value in the service's k8s-test-registry-secret.yaml


# tail logs for all pods in a deployment

Check your version of kubectl:
```
kubectl version
Client Version: version.Info{Major:"1", Minor:"19", GitVersion:"v1.19.3", GitCommit:"1e11e4a2108024935ecfcb2912226cedeafd99df", GitTreeState:"clean", BuildDate:"2020-10-14T12:50:19Z", GoVersion:"go1.15.2", Compiler:"gc", Platform:"darwin/amd64"}
Server Version: version.Info{Major:"1", Minor:"17+", GitVersion:"v1.17.12-eks-7684af", GitCommit:"7684af4ac41370dd109ac13817023cb8063e3d45", GitTreeState:"clean", BuildDate:"2020-10-20T22:57:40Z", GoVersion:"go1.13.15", Compiler:"gc", Platform:"linux/amd64"}
```

If you have the above versions or higher then you can use the new `--prefix` option, which will prepend the pod instance name to the log file.  This is very handy for quickly making sure the Load Balancer is working properly and that the pods are healthy

```
➜  ansible git:(master) ✗ kubectl logs --prefix -f -lapp=k8s-test-web -n dev-aws-peterleiser-dot-com-apps | grep Completed

[pod/k8s-test-deployment-5cb64b67d9-dhths/k8s-test-web] I, [2020-12-04T12:00:05.044500 #1]  INFO -- : [1d1dc232-e34b-4bf2-9da0-121ce3d75683] Completed 200 OK in 15ms (Views: 4.1ms | ActiveRecord: 7.2ms | Allocations: 2344)
[pod/k8s-test-deployment-5cb64b67d9-dhths/k8s-test-web] I, [2020-12-04T12:00:14.148507 #1]  INFO -- : [2bf3f569-a08d-441a-b491-4fdb09ed9b34] Completed 200 OK in 16ms (Views: 4.0ms | ActiveRecord: 7.9ms | Allocations: 2427)
[pod/k8s-test-deployment-5cb64b67d9-wrzbf/k8s-test-web] I, [2020-12-04T12:00:05.273857 #1]  INFO -- : [3c5164a8-7f5f-4933-ae8a-3375f46aafc3] Completed 200 OK in 14ms (Views: 3.8ms | ActiveRecord: 7.5ms | Allocations: 2344)
[pod/k8s-test-deployment-5cb64b67d9-wrzbf/k8s-test-web] I, [2020-12-04T12:12:29.114352 #1]  INFO -- : [99bbf458-6236-4645-bf70-1e7c47c501ab] Completed 200 OK in 31ms (Views: 4.9ms | ActiveRecord: 10.5ms | Allocations: 4367)
```

```
kubectl -n dev-aws-peterleiser-dot-com-apps logs -f deployment/k8s-test-deployment --all-containers=true
```

or

```
 kubectl logs -f -lapp=k8s-test-web -n dev-aws-peterleiser-dot-com-apps
```
