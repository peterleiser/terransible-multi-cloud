# Terraform and Ansible Top Level README

## `aws` (Amazon Web Services)

The `aws` directory contains new terraform and ansible scripts to setup the Kubernetes cluster in AWS Elastic Kubernetes Service (EKS), which is similar to Google Kubernetes Engine (GKE). They also setup and configure a PostgreSQL server running on an Amazon EC2 instance, and then deploy two Ruby on Rails applications (see below).

There's a `README` within the `aws` folder at `./aws/iac-provision-aws-eks-cluster` to explain what is done and how to operate the scripts.  

## `gcp` (Google Cloud Platform)

The `gcp` directory contains an updated version of the original DevOps test.  I wanted to reuse it for multiple environments, so now it's fairly easy to configure a new project (env_name).

I used the new version to setup a `dev2-gcp` environment and then I retired the original `dev-gcp` environment.  The new applications are here:

The Terraform and Ansible scripts are no longer hardcoded to a project name.  The project (env_name) can be specified in variables and the scripts don't need hardcoding.

I refactored it to use modules.

I refactored it to be more "DRY".  For example, it uses loops, etc where practical.

## Rails Apps

I added a couple of Rails apps to the Kubernetes cluster to demonstrate how rapidly a new or existing Rails app can be configured and deployed into the cluster.

#### My Sample Rails App:

App links:

`gcp version`:

- http://k8s-test.dev2-gcp.peterleiser.com/


`aws version`:

- http://a1a2f7abfb1de4f428ef3d8a403b3794-294262798.us-east-2.elb.amazonaws.com:3000

This application is discussed further in the `README-original-test.[md,html.pdf]` files.

#### My Covid-19 Tracker Rails App:

App links:

`gcp version`:

- http://covid19-tracker.dev2-gcp.peterleiser.com/tn/state


- http://covid19-tracker.dev2-gcp.peterleiser.com/tn/county


- http://covid19-tracker.dev2-gcp.peterleiser.com/blazer


- http://covid19-tracker.dev2-gcp.peterleiser.com/blazer/dashboards/3-all-states

`aws version`:

- http://a728d3efabf204f7db6f12893e74e899-1138385648.us-east-2.elb.amazonaws.com:3000/tn/state


- http://a728d3efabf204f7db6f12893e74e899-1138385648.us-east-2.elb.amazonaws.com:3000/tn/county


- http://a728d3efabf204f7db6f12893e74e899-1138385648.us-east-2.elb.amazonaws.com:3000/blazer


- http://a728d3efabf204f7db6f12893e74e899-1138385648.us-east-2.elb.amazonaws.com:3000/blazer/dashboards/3-all-states


Blazer is a simple but powerful database query and visualization tool that allows for rapid exploration of datasets.  New Query's can be created and saved rapidly, and also assigned to Dashboard objects.  I integrated Dashboard objects into the main county and state website views so that new Query's can immediately be added to the site.  

I wrote an ETL module that can be used, and customized, for virtually any data source (CSV, Excel, Screen Scraping, etc).  

A daily process (like Cron) can call `bundle exec rake update:all` within a Pod instance to kick off an ETL for each source.

It currently performs ETL on the following sources:
* https://github.com/nytimes/covid-19-data
* https://covidtracking.com/api
* https://www.tn.gov/health/cedep/ncov/data/downloadable-datasets.html
* https://www.census.gov/data/datasets/time-series/demo/popest/2010s-counties-total.html
* https://www.census.gov/library/reference/code-lists/ansi/ansi-codes-for-states.html

As an experiment with the PostgreSQL `Large Object` feature -- https://paquier.xyz/postgresql-2/playing-with-large-objects-in-postgres/) -- I make every run of the ETL update first store the raw, downloaded data file (CSV, Excel, HTML, etc) into PostgreSQL as a `Large Object`.  This allows for a full historical record and audit trail of data while avoiding saving files on disk.  Having applications running in the cloud that archive files to disk is definitely not in keeping with the Rails stateless architecture. Instead, they are efficiently stored centrally in PostgreSQL, and in such a way that normal data queries are not affected.  An obvious alternative is to store the raw files in AWS S3 or GCP storage buckets, but PostgreSQL was a "lowest common denominator" test of not being dependent on a particular cloud provider.

The data is then converted to CSV and batch imported to PostgreSQL.  The batch import process uses the `COPY FROM` command of the native `psql` command line tool.  It is extremely fast at bulk import and can also archive the previous tables in case of issues.  

There's even a data source that interacts with the `Microsoft Bot Framework` and interrogates it using 16 parallel processes using Parallel: https://github.com/grosser/parallel.  A journalist in Nashville reported that the the State of Tennessee was deliberately trying to hide the data of Covid-19 outbreaks in schools.  The state attempted to make it virtually impossible to collect this data statewide by only making the data available via an interactive chat bot that required more than 600 `clicks`, as well as typing, to gather the data statewide.  It was also quite deceptive on how it required the user to ask for school district data, which meant for the most part only local residents would know how to retrieve the data for certain districts.  

My interactive interrogator was able to interact with, retrieve, and reliably piece the entire state-wide data set together in about 2 or 3 minutes per run.  I then sent the data to the reporter.
