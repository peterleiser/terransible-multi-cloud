# Smart Pension DevOps Test

## Problem Statement

Using an automation tool of your choice (eg. Ansible, Puppet, Chef, Terraform or a suitable combination thereof), produce the  necessary manifests to deploy a basic web application into any cloud provider. 

The service should be logically separated into 3 distinct areas 
1.	A load balancer.
2.	The software logic responsible for serving responding to HTTP requests.
3.	A persistence backend.

You can either create a simple application yourself or make use of an existing piece of software such as Wordpress or ButterCMS (bonus points if it's a Rails application). You may make use of existing modules from a provider such as Puppet Labs or Ansible Galaxy. 

## Solution Summary

I wrote `terraform` and `ansible` scripts to setup a `Kubernetes Cluster` (k8s) that utilizes the `Kubernetes Engine` (GKE) Compute Service from `Google Cloud Platform` (GCP). The scripts also configure and deploy 2 additional virtual machines, including a PostgreSQL server for use by the applications running within the cluster (including a `Ruby on Rails k8s-test app`), and a "data store" virtual machine for additional non-relational database persistent storage. However, in the interest of time I did not also configure and deploy Redis, RabbitMQ, zookeeper, etc to this store.  Adding additional virtual machines by modifying the scripts is straightforward.  

The scripts also setup a load balancer within the k8s cluster so that any application can deploy multiple pod instances for redundancy, rolling updates, performance, etc.  I setup the k8s cluster with an nginx ingress controller that I configured as `type: LoadBalancer` to provide load balancing for apps and services (e.g. the `Ruby on Rails k8s-test app`) that have more than one `Pod` instance per app.

I wrote a simple Ruby on Rails app named `k8s-test`: http://k8s-test.dev-gcp.peterleiser.com

It provides a working, end-to-end complete life-cycle example of incorporating a new (or existing) Rails app into the DevOps process: building, managing, deploying into the k8s cluster, and how to prepare and deploy updated versions of a Rails app into the k8s cluster.  

![http://k8s-test.dev-gcp.peterleiser.com](images/2020/11/http-k8s-test-dev-gcp-peterleiser-com.png)

The `Ruby on Rails k8s-test app` also demonstrates the use of the PostgreSQL server and the load balancer mentioned above.  

For example, you can exercise the load balancer by deleting one of the `k8s-test` pod instances from the GCP console and verify that http://k8s-test.dev-gcp.peterleiser.com still serves requests.  You will see in GCP console that the deleted pod immediately begins to redeploy itself.  

You can also use the kubectl log command to `tail` the rails logs of the two `k8s-test` pods:

    kubectl logs \
      -f \
      -lapp=k8s-test-web \
      --prefix \
      --namespace=dev-gcp-peterleiser-dot-com-apps | grep Completed

And then verify that both pod instances are serving requests when they are both running.  The two pod instances end with "wzq7q" and "cf7wm":

    [pod/k8s-test-deployment-775bf9cf9b-wzq7q/k8s-test-web] I, [2020-11-16T05:28:35.120082 #1]  INFO -- : [c4da89d9940643bad49f0c095ebe58f7] Completed 200 OK in 18ms (Views: 8.4ms | ActiveRecord: 4.2ms | Allocations: 2348)
    [pod/k8s-test-deployment-775bf9cf9b-cf7wm/k8s-test-web] I, [2020-11-16T05:28:35.224321 #1]  INFO -- : [f085e096b8eafea36a36f7f417de741b] Completed 200 OK in 11ms (Views: 4.6ms | ActiveRecord: 3.2ms | Allocations: 2345)
    [pod/k8s-test-deployment-775bf9cf9b-wzq7q/k8s-test-web] I, [2020-11-16T05:28:35.340574 #1]  INFO -- : [44332f33bbe13388db7b226abfd64a2d] Completed 200 OK in 18ms (Views: 8.6ms | ActiveRecord: 4.2ms | Allocations: 2368)
    [pod/k8s-test-deployment-775bf9cf9b-wzq7q/k8s-test-web] I, [2020-11-16T05:28:35.460305 #1]  INFO -- : [3ac7ed18cd2fb31e3825b0e573d9dfe2] Completed 200 OK in 18ms (Views: 8.1ms | ActiveRecord: 4.6ms | Allocations: 2345)
    [pod/k8s-test-deployment-775bf9cf9b-cf7wm/k8s-test-web] I, [2020-11-16T05:28:35.566437 #1]  INFO -- : [4f62ab4a5a87b295ebebeeb3d3d07d52] Completed 200 OK in 12ms (Views: 4.9ms | ActiveRecord:

   You can quickly generate enough requests for this test via:

    while true
      do
        curl "http://k8s-test.dev-gcp.peterleiser.com"
    done


The "data store" is also a virtual host for static websites, including the http://info.dev-gcp.peterleiser.com portal that can be used for information about the deployment.  
* login/password: terrible/$3cur1+y
* This was only done for expediency; .htpasswd is terrible for security

![info.dev-gcp.peterleiser.com](images/2020/11/info-dev-gcp-peterleiser-com.png)

<i>Note: I didn't setup http://dash-dev-gcp.peterleiser.com.  Also, it doesn't add too much value vs. using GCP console.</i>

<i>Note: I intend to migrate my https://covid19.peterleiser.com site to this cluster after the test.  I was going to do it before/during, but I didn't have enough time.  https://covid19.peterleiser.com is currently hosted on GCP App Engine and uses Cloud SQL PostgreSQL.  Migrating it to this cluster will be much, much easier to manage.  Updates should be faster and easier, and total costs will likely be much less expensive, especially once I reduce the compute resources of `store`, `psql`, and the two k8s node virtual machines.</i>

##### The simple prerequisites for using the scripts and utilizing the cluster are:
1. Create a new project in [`Google Cloud Console`][d4bd513c] (I named it dev-gpc)
2. Register a new domain or use your existing domain (I used my existing peterleiser.com domain)
3. Decide what the subdomain name should be for your new k8s cluster (I used dev-gcp.peterleiser.com)
4. After the scripts have created the `Cloud DNS` `zone` in GCP for your subdomain (my `zone` is named `dev-gcp`), look at the NS records within your `Cloud DNS` `zone` for your subdomain (usually there are 4 NS records).  Record the NS data values (e.g. `ns-cloud-e1.googledomains.com.` `ns-cloud-e2.googledomains.com.` `ns-cloud-e3.googledomains.com.` `ns-cloud-e4.googledomains.com.`), go to the DNS hosting provider for your domain (in my case it is dreamhost.com), and add one NS record for your subdomain for each of those previous `Cloud DNS` `zone` NS data values. I added one for each:
   1. dev-gcp 	NS	ns-cloud-e1.googledomains.com.
   2. dev-gcp 	NS	ns-cloud-e2.googledomains.com.
   3. dev-gcp 	NS	ns-cloud-e3.googledomains.com.
   4. dev-gcp 	NS	ns-cloud-e4.googledomains.com.

Then the entire dev-gcp.peterleiser.com subdomain is managed by GCP `Cloud DNS`.

## Solution Details

##### The k8s cluster consists of four GCP Compute Engine VM instances:

Name  | Machine Type  | Purpose  | External URL(s)
-- |---    |-- | --
dev-gcp-store | n1-standard-4 | <li>Portal for deployment info <li>Virtual host for static websites <li>Platform services (e.g. `Redis`, `RabbitMQ`  <li>Temp storage dump files for DB data bootstrap) | <li>http://info.dev-gcp.peterleiser.com <li>http://public.store.dev-gcp.peterleiser.com
dev-gcp-psql | n1-standard-4 | PostgreSQL server for app databases  | public.psql.dev-gcp.peterleiser.com
gke-k8s-gcp-dev-gcp-ml-cluster-01-nod-ec17fa15-chcg | n1-standard-8 | k8s node | N/A
gke-k8s-gcp-dev-gcp-ml-cluster-01-nod-ec17fa15-z0pv | n1-standard-8 | k8s node | N/A

##### The k8s cluster is running the following pods:

`deployer@ubuntu-motile:~/terransible/peterleiser-dot-com/iac/dev-gcp$ kubectl --all-namespaces=true get pods  | awk -F " " '{ print $1 "|" $2 "|" $3 }'`

NAMESPACE|NAME|READY
--|--|--
default|peterleiser-dot-com-nginx-nginx-ingress-controller-755f856t9l4c|1/1
default|peterleiser-dot-com-nginx-nginx-ingress-default-backend-5dc97lq|1/1
dev-gcp-peterleiser-dot-com-apps|k8s-test-deployment-775bf9cf9b-fcmqk|1/1
dev-gcp-peterleiser-dot-com-apps|k8s-test-deployment-775bf9cf9b-x498l|1/1
kube-system|event-exporter-gke-8489df9489-shbzs|2/2
kube-system|fluentd-gke-d79b5|2/2
kube-system|fluentd-gke-kzz54|2/2
kube-system|fluentd-gke-scaler-cd4d654d7-n6t8h|1/1
kube-system|gke-metrics-agent-7ggs9|1/1
kube-system|gke-metrics-agent-lfctw|1/1
kube-system|kube-dns-7c976ddbdb-ptxtp|4/4
kube-system|kube-dns-7c976ddbdb-rj6xd|4/4
kube-system|kube-dns-autoscaler-645f7d66cf-245tg|1/1
kube-system|kube-proxy-gke-k8s-gcp-dev-gcp-ml-cluster-01-nod-ec17fa15-chcg|1/1
kube-system|kube-proxy-gke-k8s-gcp-dev-gcp-ml-cluster-01-nod-ec17fa15-z0pv|1/1
kube-system|metrics-server-v0.3.6-64655c969-qn77s|2/2
kube-system|prometheus-to-sd-bxfbz|1/1
kube-system|prometheus-to-sd-pz6pr|1/1
kube-system|stackdriver-metadata-agent-cluster-level-65f5859bd5-nd222|2/2
kube-system|tiller-deploy-7d7bc87bb-8crqr|1/1

##### The pods in the namespaces `default` and `dev-gcp-peterleiser-dot-com-apps` are of most interest.

##### `default` namespace:

> * <b>Contains the nginx ingress controller: </b>  
  * Configured as `type: LoadBalancer` to provide load balancing for apps and services (e.g. the `Ruby on Rails k8s-test app`) that have more than one `Pod` instance per app
  * Configured with `externalTrafficPolicy: Local` to [preserve the client source IP][cd724cea].  Otherwise, results from the Rails method `request.remote_ip` and values for header names like `REMOTE_ADDR`, `HTTP_X_FORWARDED_FOR`, `HTTP_CLIENT_IP`, etc will be populated with the internal IP address of the nginx ingress controller and NOT the original client remote IP address.  Additional details specifically about this are [here][48f93e56]
> * <b>Contains [default backend][217197e7]</b>:
  * Handles /healthz requests
  * Handles 404 not found requests

  [cd724cea]: https://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/#preserving-the-client-source-ip "preserve the client source IP"
  [48f93e56]: https://kubernetes.io/docs/tutorials/services/source-ip/#source-ip-for-services-with-typeloadbalancer "Setting externalTrafficPolicy to the value `Local`"

##### `dev-gcp-peterleiser-dot-com-apps	` namespace:

> * <b>Your apps, services, workers, APIs:</b> [Dockerize][9ddfca5a] your Ruby on Rails apps, Python apps, etc and run them here in one or more pods
> * <b>Simple `k8s-test` Ruby on Rails app:</b> I wrote a rails app using [Rails 6.0.3.4][bbd3a396] running [Ruby 2.7.2][0f845134].   Both Ruby and Rails versions are new as of early October 2020, and if an environment can run bleeding edge Ruby and Rails versions, it can run anything! ;-)

##### Peter Leiser's `k8s-test` Ruby on Rails app:

![http://k8s-test.dev-gcp.peterleiser.com](images/2020/11/http-k8s-test-dev-gcp-peterleiser-com.png)

> * http://k8s-test.dev-gcp.peterleiser.com
> * The Rails app has one view that:
 * Displays the remote IP address of the web browser or client making the request (which only works from setting `externalTrafficPolicy: Local` on the nginx ingress controller as explained above)
 * Displays the current time in UTC
 * Inserts a new record in the `request_logs` table in the database named `k8s_test` that is running on the PostgreSQL Server at `psql.dev-gcp.peterleiser.com`
 * Displays the `Request Time` and `Remote IP address` of the previous 20 page requests
> * Simple Ruby on Rails app using [Rails 6.0.3.4][bbd3a396] running [Ruby 2.7.2][0f845134].
> * I created a `Dockerfile` compatible with the new and exciting quirks of [Rails 6.0.3.4][bbd3a396], including:
 * <b>Explicitly recreates an empty ./tmp/pids directory.</b> A standard Dockerfile with remove the Rails ./tmp directory, but now `Puma` will not start if ./tmp/pids directory is missing
 * <b>Removes the `--without` flag for `bundle install`.</b> Otherwise, docker build will fail due to issues with the `listen` gem
 * <b>Does not delete the `./app/assets` directory.</b> Otherwise, Rails will not start due to a missing `manifest.js` file
 * <b>Installs yarn</b>. This wasn't necessary in previous major versions of Rails
> * I build and push Rails containers to `gitlab.com Container Registry`:
  * The commands are available at `CLI Commands` in your gitlab project under `Packages & Registries` -> `Container Registry`
  * Also created a gitlab `Access Token` with `scope: read_registry` in order for k8s to pull images for pod deployments
  * A build after a change to the k8s-test rails apps takes about 60 - 90 seconds, and the push takes about 10 seconds.
> * There are many ways to deploy new builds to the k8s cluster from the `Container Registry`.  
  * You can do zero-downtime updates manually by deleting one k8s-test pod instance at a time within `Google Cloud Console` -> `Kubernetes Engine` -> `Workloads`.  
      * Delete one pod instance
      * wait for the new `Container Registry` container image to be immediately deployed
      * then delete the other pod instance to have the second pod automatically deployed with the new container image.  
      * Note that this only works if the k8s-test app's Service Deployment yaml file named `k8s-test-deployment-web.yaml` specifies the image as `image: registry.gitlab.com/peterleiser/k8s_test:latest`. If you don't desire this behaviour, you can specify a specific version within the Service Deployment file, such as `image: registry.gitlab.com/peterleiser/k8s_test:1.0.3`
  * You can do a zero-downtime update by changing the image specified within the k8s-test app's Service Deployment yaml file
   * Then run `./iac/dev-gcp/8-k8s-ans/ansible-playbook peterleiser-dot-com_k8s.yml`

## Terransible: `terraform` and `ansible` scripts:

The full end-to-end process of setting up the virtual machines, the networking, the k8s cluster, and deploying applications is handled by 9 `terraform` and `ansible` subdirectories, listed below.  

The parent directory has the common config files `terraform.tfvars.common` and `ansible.cfg.common`, and a `hosts` inventory file that describes the 2 virtual machines named `store` and `psql`.  The `hosts` file is updated with IP addresses as the `terraform` and `ansible` scripts run.  Note that the `deployer` host did not end up being used, and instead my personal laptop acted at `deployer`.

Each `terraform` subdirectory:
* The name ends with `-tf`
* Contains a main.tf file
* Symlinks terraform.tfvars to ../terraform.tfvars.common
* Run `terraform apply` to perform the actions listed within main.tf

Each `ansible` subdirectory:
* The name ends with `-ans`
* Contains a main playbook file named site.yml
* May have more .yml playbook files that are used by the main site.yml playbook
* Symlinks ansible.cfg to ../ansible.cfg.common
* Run `ansible-playbook site.yml` to perform the actions listed within site.yml

Terraform documentation:
* [Google Cloud Platform ][456bc6ea]

  [456bc6ea]: https://registry.terraform.io/providers/hashicorp/google/latest/docs "Google Cloud Platform"

##### 1-init-tf

* Configures which GCP API services are allowed for use (e.g. `container`, `iam`, `compute`)

##### 2-net-tf

* Configures the `network` named dev-gcp-network and `subnet` named dev-gcp-subnet with IP range 10.16.0.0/20
* Configures the `firewall` and `ports` for k8s cluster and virtual machines
* Configures `DNS managed zone` for `dev-gcp.peterleiser.com`

##### 3-disk-tf

* Creates a `pd-ssd` persistent disk for `psql` PostgreSQL server named `disk-psql` to be used as attached disk for PostgreSQL data
* Disk includes a `lifecyle` of `prevent_destroy=true` so that destroying the PostgreSQL data must be an explicit, conscious decision

##### 4-vm-tf

* Creates the `store` compute instance virtual machine
* Creates the `psql` compute instance virtual machine and attaches the `disk-psql` persistent disk for PostgreSQL data
* dev-gcp.pub keys are added to the virtual machines for `ansible` and DevOps personnel
* External IP addresses are saved to ../hosts for later use
* Subnet and External IP addresses are associated with DNS record sets
* Needs Improvement:
  - The creation of compute instance virtual machines needs to be made modular.

##### 5-vm-ans

* Formats `disk-psql` if needed (only formats it once, so it's idempotent)
* Mounts `disk-psql` to /data on PostgreSQL virtual machine
* Installs PostgreSQL on `psql` virtual machine
  - creates one or more databases.  Needs Improvement: This should be data driven via inventory, config files, etc AND NOT HARDCODED
  - creates `dba` user and sets the password.  Needs Improvement: This is not secure.  Look into using Ansible Vault instead.
  - installs Postgis extension
  - configures PostgreSQL to be externally available.  Needs Improvement: This should be optional, as not all organizations have the need or desire to expose their database server beyond the firewall.
* Installs nginx to `store` virtual machine

##### 6-data-ans

* Sets permissions on `psql` /data directory which is mounted from `disk-psql`
* `rsync` of database dump files from ~/dbd directory to `psql` virtual machine so that they can be bootstrapped
* restore_psql.yml specifies which data dump file should be restored, and to which database.  Needs Improvement: This works, but it's error prone.  It should be data driven via configuration files to make it more modular
* command_restore_psql.yml is a module to import a particular database dump file to a particular database
* sets up nginx on `store` virtual machine for the static `info.dev-gcp.peterleiser.com` site, and transfers files from templates/info (the static site) to `/var/www/info` on `store`.  
 * Needs Improvement:
    * This should be modularized for whatever other sites are necessary.  On the plus side, this approach easily allows adding virtual host sites to be served from `store`
    * The .htpasswd was a quick means to an end and the data is not sensitive.  But this is not production-worthy; HTTPS is needed as well.
* Needs Improvement:
  - The PostgreSQL setup needs a lot of cleanup and consolidation to make it more straightforward to use

##### 7-k8s-tf

* Setup the k8s cluster with 2 nodes
* Gathers the `client_certificate`, `client_key`, and `cluster_ca_certificate`
* Uses the `google_json_credentials_location` and `google_root_project_id` (i.e. the new 'dev-gcp' project I created) to activate the services account using `gcloud` cli
* Writes the `gcloud` k8s credentials to `.kube` so `gcloud` and `kubectl` can be used later by scripts and DevOps personnel

##### 8-k8s-ans

> ###### helm.yml -> Helm
* Initializes `helm`
* Creates tiller roles from `./k8s/dev-gcp/tiller_roles.yaml`
* Uses `helm` to install `nginx-ingress`
  - name: `peterleiser-dot-com-nginx`
  - values: `./k8s/helm/nginx-ingress-values.yaml`
  - Needs Improvement:
    - Needs to be upgraded to use latest version of helm, which removed tiller
    - `stable/nginx` is deprecated and should be replaced

> ###### write_lb_ip.yml -> Load Balancer
* Writes the k8s nginx-ingress load balancer external IP address to the terraform.tfvars file for later use by `terraform`

> ###### peterleiser-dot-com_k8s.yml -> Deploy applications (i.e. `k8s-test` Rails app) as pods
* Deploys new, first-time applications to pods
* Deploys existing, updated (i.e. new docker image in `Container Registry`) to pods
* Think of it as the playbook that applies everything under `./k8s/dev-gcp/`
* Create namespaces
  * `dev-gcp-peterleiser-dot-com-apps`
  * `dev-gcp-infra`
* Find all the applications' services manifests and `kubectl apply` them
  - Each application (i.e. `k8s-test` Rails app) has a subdirectory at `./k8s/dev-gcp` (i.e. `./k8s/dev-gcp/k8s-test/`)
* Create all of the application pods by recursively applying all related manifests for each application.  In other words, apply all of the files under `./dev-gcp/`, which will apply all of the related manifests in `./k8s/dev-gcp/k8s-test/`
  - This means you can have one subdirectory for each application under `./k8s/dev-gcp/`, where each subdirectory such as `./k8s/dev-gcp/k8s-test` can contain all of it's manifest files that are logically separated into different files
* Each `./k8s/dev-gcp/` application subdirectory, such as `./k8s/dev-gcp/k8s-test/` follows the pattern where each manifest file is prefixed by the application service name (e.g. `k8s-test-`).  And each application has the following manifest yaml files:
  - `configmap`
    - non-secret env variables
  - `deployment-web`
    - specify number of replicas (i.e. # of pods for redundancy)
    - maps/sets the `secrets` as env variables
    - specifies the `docker image` to pull and from what `Container Registry`
    - specifies the compute resources to be used by the pod(s)
  - `ingress`
    - host names, service names
  - `registry-secret`
    - gitlab `Container Registry` credentials to pull images.  This is the token mentioned above
  - `secrets`
    - `kind: Secret` and `type: Opaque`
    - base64 encoded secrets, like passwords, license keys, Rails secret_base_key
  - `service`
    - boilerplate
* Needs Improvement:
  - Once you know how to generate `secrets` and `registry-secret` it's pretty straightforward.  But to people unfamiliar with the concepts or process it's confusing.  So better documentation that very, very concretely documents every step and uses `k8s-test-secrets.yaml` and `k8s-test-registry-secret` as examples would be useful
  - There's a lot of tedious updates to service names, etc to relate all of the manifests together.  This is very tedious and error prone.  The manifests are mostly boilerplate templates, so this should be automated into a script/module that takes the small number of actual input variables and applys them to generate/update the manifest files.

> ###### helm_datadog.yml -> DataDog
  * Installs and configures DataDog into the cluster
  * I didn't use this since I didn't setup DataDog for dev-gcp.peterleiser.com, but I've used it before and it's very useful (and much, much, much less expensive than New Relic)

##### 9-net-tf

* Creates the `A DNS Record` for `*.dev-gcp.peterleiser.com`
* You should now be able to kick the tires on your shiny new k8s cluster, with shiny new apps, proper URLs, redundancy and reliability, etc

## Improvements

In addition to the "Needs Improvement" sections embedded above, the entire system need a thorough security audit.  HTTPS is one obvious need, but I didn't have time for that unfortunately. Cloudflare is an option, but it really depends on requirements, cost, risk assessment.  Need to make sure PostgreSQL has secure connections with SSL.  Need to clean up the password management with PostgreSQL as well.

A security audit around the Kubernetes certificates and credentials, ssh key access to vm's, is necessary and needs to be incorporated into an organizational best practices on how to handle keys, passwords, credentials, and who is allowed access to which systems.

Needs monitoring and alerting, and integration to PagerDuty.

Needs the documentation to include a FAQ and/or HOW-TO's for operating things, dependencies, making changes.  Otherwise people become afraid to touch things and change things and upgrade and improve things.  And then it's technical debt city.


  [217197e7]: https://kubernetes.github.io/ingress-nginx/user-guide/default-backend/ "nginx default backend"
  [9ddfca5a]: https://docs.docker.com/samples/ "Dockerize"
  [0f845134]: https://www.ruby-lang.org/en/downloads/releases/ "Ruby 2.7.2"
  [bbd3a396]: https://rubyonrails.org/ "Rails 6.0.3.4"
