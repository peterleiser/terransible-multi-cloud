module "common" {
  source = "../modules/common"
}

output "common_config" {
  value = module.common.config
}
