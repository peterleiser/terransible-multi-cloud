module "common" {
  source = "../modules/common"
}

# Initiate Google provider
provider "google" {
  version     = "~> 3.47.0"
  credentials = file(module.common.config.google_json_credentials_location)
  project     = module.common.config.google_root_project_id
  region      = module.common.config.google_region
  zone        = module.common.config.google_zone
}

resource "google_compute_disk" "psql" {
  name  = "disk-psql"
  type  = "pd-ssd"

  lifecycle {
    prevent_destroy = true
  }
}

