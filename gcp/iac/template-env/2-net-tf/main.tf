module "common" {
  source = "../modules/common"
}

# Initiate Google provider
provider "google" {
  version     = "~> 3.47.0"
  credentials = file(module.common.config.google_json_credentials_location)
  project     = module.common.config.google_root_project_id
  region      = module.common.config.google_region
  zone        = module.common.config.google_zone
}

# Disabling creating a new network so we keep default firewall rules
resource "google_compute_network" "vpc_network" {
  name                    = "${module.common.config.env_name}-network"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "vpc_subnet" {
  network       = google_compute_network.vpc_network.name
  name          = "${module.common.config.env_name}-subnet"
  ip_cidr_range = "10.16.0.0/20"
  region        = "us-central1"
}

module "icmp" {
  source = "../modules/firewall"

  name = "icmp"
  protocols = {
    "imcp" = {}
  }
}

module "internal" {
  source = "../modules/firewall"

  name = "icmp"
  protocols = {
    "icmp" = {},
    "tcp" = {ports = ["0-65535"]},
    "udp" = {ports = ["0-65535"]}
  }
  source_ranges = ["10.128.0.0/9"]
}

module "rdp" {
  source = "../modules/firewall"

  name = "icmp"
  protocols = {
    "tcp" = {ports = ["3389"]},
  }
  source_ranges = ["10.128.0.0/9"]
}

module "ssh" {
  source = "../modules/firewall"

  name = "ssh"
  protocols = {
    "tcp" = {ports = ["22"]},
  }
}

module "open_store_ports" {
  source = "../modules/firewall"

  name = "store"
  protocols = {
    # http, psql, mysql, redis, zookeeper, memcache, rabbitmq
    "tcp" = {ports = [80, 5432, 3306, 6379, 2181, 11211, 5672, 15672]},
  }
  target_tags   = ["store", "psql"]
}

## setup dns zone
# NOTE: We're not taking advantage of the private DNS for now so there's no need to
#       assign it to our previously setup network yet
resource "google_dns_managed_zone" "env_name" {
  name     = module.common.config.env_name
  dns_name = "${module.common.config.env_name}.peterleiser.com."
}
