module "common" {
  source = "../modules/common"
}

# Initiate Google provider
provider "google" {
  version     = "~> 3.47.0"
  credentials = file(module.common.config.google_json_credentials_location)
  project     = module.common.config.google_root_project_id
  region      = module.common.config.google_region
  zone        = module.common.config.google_zone
}

####################################################################################################
# Setup the cluster and node pool.
#
# Then then activate the service account in order to use gcloud:
# (https://cloud.google.com/sdk/gcloud/reference/auth/activate-service-account)
#
# And also retrieve & write those GCP credentials to ~/.kube/config in order to also use kubectl
####################################################################################################

resource "google_container_cluster" "primary_node_pool" {
  name       = module.common.config.k8s_name
  network    = "${module.common.config.env_name}-network"
  subnetwork = "${module.common.config.env_name}-subnet"

  location           = module.common.config.google_zone
  initial_node_count = 1

  master_auth {
    username = ""
    password = ""
  }

  #min_master_version       = "1.13.7-gke.8"
  remove_default_node_pool  = "true"

  addons_config {
    http_load_balancing {
      disabled = true
    }
  }
}

resource "google_container_node_pool" "real_node_pool" {
  name       = "${module.common.config.env_name}-ml-cluster-01-node-pool"
  location   = module.common.config.google_zone
  cluster    = google_container_cluster.primary_node_pool.name
  node_count = 2

  node_config {
    preemptible  = false
    machine_type = "n1-standard-2"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

output "client_certificate" {
  value = google_container_cluster.primary_node_pool.master_auth.0.client_certificate
}

output "client_key" {
  value = google_container_cluster.primary_node_pool.master_auth.0.client_key
}

output "cluster_ca_certificate" {
  value = google_container_cluster.primary_node_pool.master_auth.0.cluster_ca_certificate
}

# Write kubeconf
# NOTE: Will overwrite ~/.kube/config , set KUBECONFIG environment variable to use different kubeconfig
#       TBH, probably setting KUBECONFIG would be better in this case in both gcloud and docean
# TODO: Move the setup-gcloud resource to a different step
resource "null_resource" "setup-gcloud" {
  depends_on = [google_container_node_pool.real_node_pool]

  provisioner "local-exec" {
    command = "gcloud auth activate-service-account --key-file ${module.common.config.google_json_credentials_location}; gcloud config set project ${module.common.config.google_root_project_id}; "
  }
}

resource "null_resource" "ansible-provision" {
  depends_on = [null_resource.setup-gcloud]

  provisioner "local-exec" {
    command = "mkdir -p ~/.kube/; gcloud container clusters get-credentials ${module.common.config.k8s_name} --zone=${module.common.config.google_zone}"
  }
}
