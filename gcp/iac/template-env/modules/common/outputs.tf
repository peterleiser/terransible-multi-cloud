output "config" {
  value = {
    k8s_name = "k8s-gcp"

    env_name = "dev2-gcp"

    # google gcp
    gc_domain_name = "dev2-gcp.peterleiser.com"
    gc_store_domain_name = "store.dev2-gcp.peterleiser.com"
    google_json_credentials_location = "~/.ssh/dev2-gcp_gcloud.json"
    google_root_project_id = "dev2-gcp"
    google_region = "us-central1"
    google_zone = "us-central1-a"
    google_project_public_key_location = "~/.ssh/dev2-gcp.pub"
    google_project_ssh_username = "deployer"
  }
}
