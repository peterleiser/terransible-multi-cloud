module "common" {
  source = "../common"
}

resource "google_compute_firewall" "rule" {
  name      = "${google_compute_network.vpc_network.name}-allow${var.name}"
  network   = google_compute_network.vpc_network.name
  direction = "INGRESS"
  priority  = "65534"

  allow {
    for_each = toset(var.protocols)

    protocol = each.key
    ports = try(each.value["ports"], null)

  }

  target_tags   = []
  source_ranges = var.source_ranges
}
