variable "name" {}

variable "protocols" {
  default = {}
}

variable "source_ranges" {
  default = ["0.0.0.0/0"]
}

variable "target_tags" {
  default = []
}
