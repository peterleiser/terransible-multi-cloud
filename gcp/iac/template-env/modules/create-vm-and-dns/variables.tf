variable "name" {}

variable "machine_type" {
  default = "n1-standard-1"
}

variable "boot_disk_image" {
  default = "ubuntu-1604-lts"
}

variable "additional_dns" {
  default = []
}

variable "attached_disks" {
  default = []
}

