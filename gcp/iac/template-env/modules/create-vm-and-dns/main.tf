module "common" {
  source = "../common"
}

resource "google_compute_instance" "vm_instance" {
  name         = "${module.common.config.env_name}-${var.name}"
  machine_type = var.machine_type
  tags         = [var.name]

  boot_disk {
    initialize_params {
      image = var.boot_disk_image
      size  = 100
      type  = "pd-ssd"
    }
  }

  network_interface {
    network       = "${module.common.config.env_name}-network"
    subnetwork    = "${module.common.config.env_name}-subnet"
    access_config {}
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/${module.common.config.env_name}.pub")}"
  }
}

# Attach any additional disks
resource "google_compute_attached_disk" "vm_attached_disk" {
  for_each = toset(var.attached_disks)
  disk = each.key
  instance = google_compute_instance.vm_instance.self_link
}

# Add public dns record
resource "google_dns_record_set" "public" {
  name         = "public.${var.name}.${module.common.config.gc_domain_name}."
  managed_zone = module.common.config.env_name
  type         = "A"
  ttl          = 300

  rrdatas = [google_compute_instance.vm_instance.network_interface.0.access_config.0.nat_ip]
}

# Add internal dns record
resource "google_dns_record_set" "internal" {
  name         = "${var.name}.${module.common.config.gc_domain_name}."
  managed_zone = module.common.config.env_name
  type         = "A"
  ttl          = 300

  rrdatas = [google_compute_instance.vm_instance.network_interface.0.network_ip]
}

# Add and additional public dns records
resource "google_dns_record_set" "additional_dns" {
  for_each = toset(var.additional_dns)

  name         = "${each.key}.${module.common.config.gc_domain_name}."
  managed_zone = module.common.config.env_name
  type         = "A"
  ttl          = 300

  rrdatas = [google_compute_instance.vm_instance.network_interface.0.access_config.0.nat_ip]
}
