module "common" {
  source = "../modules/common"
}

# Initiate Google provider
provider "google" {
  version     = "~> 3.47.0"
  credentials = file(module.common.config.google_json_credentials_location)
  project     = module.common.config.google_root_project_id
  region      = module.common.config.google_region
  zone        = module.common.config.google_zone
}

#-- build 1.store vm

module "store" {
  source = "../modules/create-vm-and-dns"

  name            = "store"
  machine_type    = "custom-1-1024"
  additional_dns  = [
    "info"
  ]
}

#-- build 2.psql vm

module "psql" {
  source = "../modules/create-vm-and-dns"

  name            = "psql"
  attached_disks  = [
    "disk-psql"
  ]
}

#---

# write hosts file for store
# This adds the external IP addresses of each vm to the ansible hosts file
data "template_file" "anshosts" {
  template = file("./templates/hosts.tpl")

  vars = {
    store_info    = "${module.store.vm_instance.network_interface.0.access_config.0.nat_ip} ansible_python_interpreter=/usr/bin/python3"
    psql_info     = "${module.psql.vm_instance.network_interface.0.access_config.0.nat_ip} ansible_python_interpreter=/usr/bin/python3"
  }
}

resource "local_file" "host_file" {
  depends_on = [module.store.vm_instance]

  content  = data.template_file.anshosts.rendered
  filename = "../hosts"
}

# Just informational, this is the external IP address of the store vm
output "store_public_ip" {
  value = module.store.vm_instance.network_interface.0.access_config.0.nat_ip
}
