module "common" {
  source = "../modules/common"
}

module "load-balancer" {
  source = "../modules/load-balancer"
}

# Initiate Google provider
provider "google" {
  version     = "~> 3.47.0"
  credentials = file(module.common.config.google_json_credentials_location)
  project     = module.common.config.google_root_project_id
  region      = module.common.config.google_region
  zone        = module.common.config.google_zone
}

resource "google_dns_record_set" "wildcard-dns-for-env" {
  name         = "*.${module.common.config.gc_domain_name}."
  managed_zone = module.common.config.env_name
  type         = "A"
  ttl          = 300

  rrdatas = [module.load-balancer.gc_lb_ip]
}
