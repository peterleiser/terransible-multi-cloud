module "common" {
  source = "../modules/common"
}

resource "google_project" "new_env" {
  name       = module.common.config.env_name
  project_id = module.common.config.env_name
  #org_id     = "1234567"
}

# Initiate Google provider
provider "google" {
  version     = "~> 3.47.0"
  credentials = file(module.common.config.google_json_credentials_location)
  project     = google_project.new_env
  region      = module.common.config.google_region
  zone        = module.common.config.google_zone
}

# These have no dependencies
resource "google_project_service" "initial" {
  for_each = toset([
    "cloudresourcemanager",
    "container"
  ])

  project = module.common.config.google_root_project_id
  service = "${ each.key }.googleapis.com"
}

# These depend on cloudresourcemanager
resource "google_project_service" "additional" {
  for_each = toset([
    "iam",
    "compute",
    "dns"
  ])

  project = module.common.config.google_root_project_id
  service = "${ each.key }.googleapis.com"

  depends_on = [google_project_service.initial]
  disable_dependent_services = true
}
